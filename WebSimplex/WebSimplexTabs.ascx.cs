﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace WebSimplex
{
    /// <summary>
    /// <p>
    /// Evento che viene scatenato in conseguenza della selezione del TAB, una volta
    /// E' stato introditto per poter consentire il salvataggio di uno o più oggetti
    /// in modo che se ne conservi lo stato durante il PostBack.
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 10/10/2015 da Casa
    /// </pre>
    /// </summary>
    /// <param name="p_sender"></param>
    /// <param name="p_e"></param>
    public delegate void WebSimplex_ChangedTabHandler(Object p_sender, EventArgs p_e);

    /// <summary>
    /// Evento che viene scatenato in conseguenza della selezione del TAB e consente 
    /// di controllare lo stato del componente WebSimplexTab tra l'evento del click sulla linguetta
    /// ed il conseguente cambio della View da visualizzare.
    /// </summary>
    /// <param name="p_sender"></param>
    /// <param name="p_e"></param>
    public delegate void WebSimplex_BeforeTabChangeHandler(Object p_sender, EventArgs p_e);

    /// <summary>
    /// Classe che rappresenta una sequenza di linguette per cartelle multiple (Tabs).
    /// Il formato grafico delle linguette è determinato dalle seguenti classi CSS:
    /// table.tabMenu
    /// tr.tabMenuRow
    /// td.tabMenuCellSelected
    /// td.tabMenuCellUnselected
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 
    /// Mdfd: 20/10/2015 - introduzione dell'evento WebSimplex_ChangeTabsHandler - v. 1.1.0.1
    /// Mdfd: 21/11/2020 - introduzione metodo pubblico setActiveView(int) per selezionare la View Attiva - v.1.1.0.3
    /// </pre>
    /// </summary>
    public partial class WebSimplexTabs : System.Web.UI.UserControl
    {
        /// <summary>
        /// Evento di cambiamento della scheda per pressione del link, dopo il cambio di scheda
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/10/2015
        /// </pre>
        /// </summary>
        public event WebSimplex_ChangedTabHandler ChangeTab;

        /// <summary>
        /// Evento di cambiamento della scheda per pressione del link, prima del cambio di scheda,
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/10/2015
        /// </pre>
        /// </summary>
        public event WebSimplex_BeforeTabChangeHandler BeforeTabChange;
        
        /// <summary>
        /// Struttura di supporto dati interna
        /// </summary>
        private struct MyInternalTabElement
        {
            public String TabName;
            public int TabIdx;
            public Boolean TabSelected;
        }
        
        /// <summary>
        /// Lista di elementi di struttura MyInternalTabElement
        /// </summary>
        private List<MyInternalTabElement> MyList = null;

        /// <summary>
        /// Controllo MultiViews Associato.
        /// </summary>
        protected System.Web.UI.WebControls.MultiView MyViews = null;

        /// <summary>
        /// Nome del controllo
        /// </summary>
        protected String MyNome;

        /// <summary>
        /// Tab selezionato di default.
        /// </summary>
        protected Int32 MyTabSelected;

        /// <summary>
        /// Lunghezza della stringa del nome della linguetta.
        /// Default 10
        /// </summary>
        protected Int32 MyTabLenght = 10;

        #region PROPRERTIES
        /// <summary>
        /// Imposta ed ottiene la lunghezza massima dell'etichetta della linguetta dei TAB
        /// </summary>
        public Int32 TabLenght
        {
            get
            {
                return MyTabLenght;
            }

            set
            {
                if (value == null)
                    MyTabLenght = 13;
                else
                    MyTabLenght = value + 3;
            }
        }

        /// <summary>
        /// Imposta e legge (RW) il controllo MultiView associato al TAB.
        /// @MdN
        /// ----
        /// Crtd: 27/01/2015
        /// Mdfd: 28/11/2020 - v.1.1.0.4 - trasformata da WO in RW
        /// </summary>
        public MultiView AssociatedMultiView
        {
            set
            {
                if (value == null)
                    return;
                MyViews = (MultiView)value;
            }
            get
            {
                //<added @MdN: 28/11/2020 - v.1.1.0.4>
                return MyViews;
            }
        }

        /// <summary>
        /// Imposta o ottiene il nome del controllo.
        /// @MdN
        /// ----
        /// Crtd: 27/01/2015
        /// </summary>
        public String Name
        {
            get
            {
                return MyNome;
            }

            set
            {
                MyNome = value;
            }
        }

        /// <summary>
        /// Ottiene o imposta il tab selezionato per default.
        /// @MdN
        /// ----
        /// Crtd: 27/01/2015
        /// Mdfd: 22/11/2020 - Memorizzazione del valore nella ViewState
        /// </summary>
        public Int32 DefaultTabSelected
        {
            get
            {   if(ViewState[ID + "_DefaultTabSelected"]!=null)
                    MyTabSelected = (Int32)ViewState[ID + "_DefaultTabSelected"];
                return MyTabSelected;
            }
            set
            {
                MyTabSelected = value;
                ViewState[ID + "_DefaultTabSelected"] = MyTabSelected; //<added @MdN: 22/11/2020>
            }
        }
        #endregion

        #region METODI PUBBLICI
        /// <summary>
        /// Accoda un nuovo tab.
        /// @MdN
        /// ----
        /// Crtd: 26/01/2015
        /// Mdfd: 27/01/2015
        /// </summary>
        /// <param name="p_TabName">Nome del Tab (nome visualizzato)</param>
        public int Add(String p_TabName)
        {
            if (p_TabName == null)
                return 0;
            // Accorcio il nome
            if (p_TabName.Length > 13)
                p_TabName = p_TabName.Substring(0, 10) + "...";
            if (MyList == null)
                MyList = new List<MyInternalTabElement>();
            MyInternalTabElement _elem = new MyInternalTabElement();
            _elem.TabIdx = MyList.Count;
            _elem.TabName = p_TabName;
            // elezione di default
            if ((MyList.Count) == MyTabSelected)
                _elem.TabSelected = true;
            else
                _elem.TabSelected = false;

            MyList.Add(_elem);
            return MyList.Count;
        }
        
        /// <summary>
        /// Cerca e restituisce la posizione (0 based) del tab selezionato.
        /// @MdN
        /// ----
        /// Crtd: 27/01/2015
        /// </summary>
        /// <returns>Numero d'ordine del tab selezionato o -1 in caso di errore.</returns>
        public Int32 getSelectedTab()
        {
            Int32 _t32 = 0;
            foreach (MyInternalTabElement x in MyList)
            {
                if (x.TabSelected == true)
                    return _t32;
                _t32++;
            }
            return -1;
        }

        /// <summary>
        /// Restituisce il numero d'ordine del tab il cui nome è passato come argomento al metodo.
        /// @Mdn
        /// ----
        /// Crtd: 29/01/2015
        /// </summary>
        /// <param name="p_Name">Nome del tab</param>
        /// <returns>il numero d'ordine del tab 0 -1 in caso di errore.</returns>
        public Int32 getTabByName(String p_Name)
        {
            Int32 _t32 = -1;

            if (p_Name == null)
                return _t32;

            _t32 = 0;
            foreach (MyInternalTabElement x in MyList)
            {
                if (x.TabName.CompareTo(p_Name) == 0)
                    return _t32;
                _t32++;
            }
            return -1;
        }

        /// <summary>
        /// Imposta il tab selezionato.
        /// @MdN
        /// ---
        /// Crtd: 27/01/2015
        /// Mdfd: 17/05/2015
        /// </summary>
        /// <param name="p_Tab">Numero d'ordine del tab da selezionare.</param>
        public void setSelectedTab(Int32 p_Tab)
        {
            Int32 _t32;
            MyInternalTabElement x = MyList[0];
            if (MyList == null)
                return;

            if (p_Tab < 0)
                return;

            //itero lungo la lista dei Tab alla ricerca dell'elemento in posizione p_Tab (0-based)
            for (_t32 = 0; _t32 < MyList.Count;_t32++ )
            {
               
                //cancella o setta
                try
                {

                    x = MyList[_t32];
                    if (_t32 == p_Tab)
                        x.TabSelected = true;
                    else
                        x.TabSelected = false; //CANCELLA LA VECCHIA SELEZIONE 
                    MyList[_t32] = x;           //@MdN 17/05/2015
                }
                catch (Exception E)
                {
                    return;
                }

            }
        }//fine

        /// <summary>
        /// Imposta il tab selezionato.
        /// @MdN
        /// ---
        /// Crtd: 29/01/2015
        /// </summary>
        /// <param name="p_Name">Nome del tab da selezionare.</param>
        public void setSelectedTab(String p_Name)
        {
            if (p_Name == null)
                return;
            setSelectedTab(getTabByName(p_Name));
        }

        /// <summary>
        /// Imposta la vista attiva.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/11/2020 - v.1.1.0.3
        /// </pre>
        /// </summary>
        /// <param name="p_tab">indice della view da attivare - Tab attivo.</param>
        public void setActiveView(int p_tab)
        {
            MyViews.ActiveViewIndex = p_tab;
        }

        #endregion

        #region PROTECTED METHODS
        /// <summary>
        /// <p>
        /// Compone graficamente la sequenza dei tab.
        /// Il tab selezionato viene letto direttamente dallo stato degli
        /// oggetti di classe MyInternalTabElement.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 17/05/2015 - Allineamento con la versione precedente
        /// </pre>
        /// </summary>
        protected virtual void designTabs()
        {
            Literal _Ltrl = null;
            LinkButton _LnkB = null;

            //Controllo
            if (MyList == null)
                return;

            //@MdN 30/04/2015 - cancello tutto
            this.Controls.Clear();

            _Ltrl = new Literal();
            _Ltrl.Text = "<table class=\"tabMenu\"><tr class =\"tabMenuRow\">";
            if (MyList == null)
                return;
            this.Controls.Add(_Ltrl);
            foreach (MyInternalTabElement _MITE in MyList)
            {
                _Ltrl = new Literal();
                if (_MITE.TabSelected == true)
                    _Ltrl.Text = "<td class=\"tabMenuCellSelected\">";
                else
                    _Ltrl.Text = "<td class=\"tabMenuCellUnSelected\">";
                this.Controls.Add(_Ltrl);
                _LnkB = new LinkButton();
                _LnkB.Text = _MITE.TabName;
                _LnkB.Click += SomeLinkHasBeenClicked;
                _LnkB.ID = "VW_" + _MITE.TabIdx.ToString();
                Controls.Add(_LnkB);
                _Ltrl = new Literal();
                _Ltrl.Text = "</td>";
                Controls.Add(_Ltrl);
            }
            _Ltrl = new Literal();
            _Ltrl.Text = "</tr></table>";
            Controls.Add(_Ltrl);
        }

        /// <summary>
        /// <p>
        /// Compone graficamente la sequenza dei tab.
        /// Il tab selezionato è quello il cui indice è passato come parametro.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ??
        /// Mdfd: 30/04/2015 - Aggiunto il parametro p_selected perchè l'informazione viene persa.
        /// </pre>
        /// </summary>
        /// <param name="p_selected">indice del tab selezionato</param>
        protected virtual void designTabs(int p_selected)
        {
            Literal _Ltrl = null;
            LinkButton _LnkB = null;
            Boolean _bool = false;
            int _count = 0;

            //Controllo
            if (MyList == null)
                return;

            //@MdN 30/04/2015 - cancello tutto
            this.Controls.Clear();


            //Inizio il disegno del compinente
            _Ltrl = new Literal();
            _Ltrl.Text = "<table class=\"tabMenu\"><tr class =\"tabMenuRow\">";
            this.Controls.Add(_Ltrl);
            foreach (MyInternalTabElement _MITE in MyList)
            {
                _Ltrl = new Literal();
                //@MdN 30/04/2015: OKKIO!!! 
                if (_count == p_selected)
                {
                    _Ltrl.Text = "<td class=\"tabMenuCellSelected\">";
                }
                else
                    _Ltrl.Text = "<td class=\"tabMenuCellUnSelected\">";
                _count++;
                this.Controls.Add(_Ltrl);
                _LnkB = new LinkButton();
                _LnkB.Text = _MITE.TabName;
                _LnkB.Click += SomeLinkHasBeenClicked;
                _LnkB.ID = "VW_" + _MITE.TabIdx.ToString();
                Controls.Add(_LnkB);
                _Ltrl = new Literal();
                _Ltrl.Text = "</td>";
                Controls.Add(_Ltrl);
            }
            _Ltrl = new Literal();
            _Ltrl.Text = "</tr></table>";
            Controls.Add(_Ltrl);
        }

        /////// <summary>
        /////// <p>
        /////// Compone graficamente la sequenza dei tab.
        /////// Il primo tab è quello selezionato di default.
        /////// </p>
        /////// <pre>
        /////// ----
        /////// @MdN
        /////// Crtd: 30/04/2015 
        /////// </pre>
        /////// </summary>
        ////protected virtual void designTabs()
        ////{
        ////    designTabs(0);
        ////}



        protected void Page_Load(object sender, EventArgs e)
        {
            // designTabs();
            Int32 _tab = 0;

            //<added @MdN: 23/11/2020 v.1.1.0.3>
            if (ViewState[ID + "_DefaultTabSelected"] != null)
            {
                // Quale tab visualizzare?
                _tab = (Int32) ViewState[ID + "_DefaultTabSelected"];
                designTabs(_tab);
                if (MyViews != null && MyViews.Views != null && MyViews.Views.Count > _tab)
                {
                    // seleziona la View
                    MyViews.ActiveViewIndex = _tab;
                }
            }
            else
            {
                designTabs();
            }
            //</added @MdN: 23/11/2020 v.1.1.0.3>
        }

        /// <summary>
        /// Metodo che gestisce l'espansione o l'oscuramento delle sezioni del
        /// controllo MultiView associato in risposta al comando Click.
        /// @MdN
        /// ----
        /// Crtd: 29/01/2015
        /// </summary>
        /// <param name="sender">Oggetto che ha inviato l'evento Click</param>
        /// <param name="e">Argomenti</param>
        protected virtual void SomeLinkHasBeenClicked(object sender, EventArgs e)
        {
            Int32 _t32 = 0;
            String Name;

            Name = ((LinkButton)sender).Text;
            _t32 = getTabByName(Name);
            
            setSelectedTab(_t32);

            // chiama l'evento 20/10/2015
            EventArgs _e = new EventArgs();
            if (BeforeTabChange != null)
                BeforeTabChange(this, _e);

            #region TEST
            //this.SaveViewState();
            // designTabs(); //<deleted @MdN: 23/11/2020>
            
            //<added @MdN: 23/11/2020>
            designTabs(_t32);
            DefaultTabSelected = _t32;
            //</added @MdN: 23/11/2020>

            this.MyViews.ActiveViewIndex = _t32;

            // chiama l'evento 20/10/2015
            EventArgs _e2 = new EventArgs();
            if(ChangeTab != null)
                ChangeTab(this, _e2);
            #endregion

            #region [ADD YOUR CODE HERE]
            #endregion
        }
        #endregion
    }
}