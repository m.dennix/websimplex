﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using simplex_FORM;

namespace WebSimplex
{
    /// <summary>
    /// <p>
    /// Controllo web che rappresenta le briciole di pane.
    /// </p>
    /// <pre>
    /// @MdN
    /// -----
    /// Crtd: 13/01/2015 (version 1.0)
    /// Mdfd: 22/03/2015 (version 1.1)
    /// Mdfd: 17/05/2016 (version 1.2): aggiunti getNameAt(int) e getPositionOf(String).
    /// </pre>
    /// 
    /// </summary>
    public partial class WebSimplexBreadCrumbs : System.Web.UI.UserControl
    {
        #region ATTRIBUTI
        /// <summary>
        /// Collezione SimplexBreadCrumbs di riferimento.
        /// @MdN
        /// ----
        /// Crtd: 13/01/2015
        /// </summary>
        protected simplex_FORM.SimplexBreadCrumbs MySbc = null;

        /// <summary>
        /// Nome del controllo. Coincide con il 
        /// nome della variabile di sessione atta a contenere la collezione nel passaggio 
        /// tra le maschere.
        /// @MdN
        /// ----
        /// Crtd: 13/01/2015
        /// </summary>
        protected String MyNome = null;

        /// <summary>
        /// Link alla pagina/form che costituisce il nodo principale (HOME) della catena
        /// di breadcrumbs.
        /// @MdN
        /// ----
        /// Crtd: 13/01/2014
        /// </summary>
        protected String MyHomeLink = null;

        /// <summary>
        /// Nome del LinkButton che è stato selezionato.
        /// @MdN
        /// ----
        /// Crtd 15/01/2015
        /// </summary>
        protected String MyCheckedButtonName = null;
        #endregion

        #region PROPERTIES
        /// <summary>
        /// Imposta ed ottiene il nome del controllo che rappresenta anche il nome della
        /// variabile di sessione che memorizza la collezione SimplexBreadCrumbs
        /// @MdN
        /// ----
        /// Crtd: 13/01/2014 
        /// </summary>
        public String Nome
        {
            get
            {
                return MyNome;
            }

            set
            {
                MyNome = value;
            }
        }

        /// <summary>
        /// Ottiene o imposta l'URL della pagina/form iniziale (HOME)
        /// @MdN
        /// ----
        /// Crtd: 14/01/2015
        /// </summary>
        public String HomeUrl
        {
            get
            {
                return MyHomeLink;
            }
            set
            {
                MyHomeLink = value;
            }
        }

        /// <summary>
        /// Conta gli elementi della catena BreadCreumbs.
        /// Se 0 la catena è vuota.
        /// </summary>
        public int Count
        {
            get
            {
                if(MySbc==null)
                    MySbc = (simplex_FORM.SimplexBreadCrumbs)(Session[MyNome]);
                if (MySbc == null)
                    return -1;
                return MySbc.Count;
            }
        }


        /// <summary>
        /// Ottiene (RO) il nome dell'elemento della catena BreadCrumbs che è stato selezionato.
        /// @MdN
        /// ----
        /// Crtd: 15/01/2015
        /// </summary>
        public String CheckedLinkButtonName
        {
            get
            {
                return MyCheckedButtonName;
            }
        }

        /// <summary>
        /// <p>Restituisce il link della pagina precedente alla pagina corrente.</p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd 21/04/2015
        /// </pre>
        /// 
        /// </summary>
        public String PreviousLink
        {
            get
            {
                return MySbc.getPreviousLink();
            }
        }

        #endregion

        #region GESTORI

        public virtual void SomeButtonHasBeenClicked(object sender, EventArgs e)
        {
            object _toSend = sender;
            Response.Redirect(((LinkButton)sender).PostBackUrl);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Caricamento o creazione di una collezione SimplexBreadCrumbs
            if ((MySbc = (simplex_FORM.SimplexBreadCrumbs)(Session[MyNome])) == null)
            {
                MySbc = new simplex_FORM.SimplexBreadCrumbs();
                MySbc.Add("HOME", MyHomeLink);
                Session[MyNome] = MySbc;
            }

            //Aggiunta dei controlli.
            // 1) Aggiungere un controllo Hyperlink per ogni elemento della catena
            int _t = 0;
            Boolean _first = true;
            for (_t = 0; _t < MySbc.Count; _t++)
            {
                // 2) Antepone il separatore.
                if (_first == false)
                {
                    Literal _Ltrl = new Literal();
                    _Ltrl.Text = MySbc.Separator;
                    _Ltrl.ID = "R_Ltr" + _t.ToString();
                    this.Controls.Add(_Ltrl);
                }
                LinkButton _Hprlnk = new LinkButton();
                _Hprlnk.CssClass = "BreadCrumbLnk";
                _Hprlnk.ID = "R_" + MySbc.getName(_t);
                _Hprlnk.Text = MySbc.getName(_t);
                _Hprlnk.PostBackUrl = MySbc.getLink(_t);
                _Hprlnk.Attributes["runat"] = "server";
                _Hprlnk.Click += new EventHandler(SomeButtonHasBeenClicked);
                _Hprlnk.Visible = true;
                this.Controls.Add(_Hprlnk);
                _first = false;
            } // fine ciclo
        } // fine

        #endregion

        #region METODI_PUBBLICI

        /// <summary>
        /// Restituisce il nome della pagina che si trova alla posizione specificata dal parametro <i>p_posizione</i>
        /// all'interno della catena di breadcrumbs.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/05/2016
        /// </pre>.
        /// </summary>
        /// <param name="p_posizione">Indice posizionale della stringa da richiamare.</param>
        /// <returns>Il nome di una pagina o null</returns>
        public String getNameAt(int p_posizione)
        {
            return MySbc.getName(p_posizione);
        }

        /// <summary>
        /// Restituisce l'indice posizionale della pagina che ha il nome passato come parametro.<br></br>
        /// E' il duale di getNameAt(int).<br></br>
        /// 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/05/2016
        /// </pre>
        /// </summary>
        /// <param name="p_Name">Nome della pagina.</param>
        /// <returns>L'indice posizionale della pagina nella catena di breadcrumbs o -1 in caso di assenza.</returns>
        public int getPositionOf(String p_Name)
        {
            // * Controllo
            if (p_Name == null)
                return -1;

            // * Controllo
            if (MySbc == null)
                return -1;

            // Esecuzione
            int _count = MySbc.Count;
            for (int _t = 0; _t < _count; _t++)
            {
                if (p_Name.CompareTo(MySbc.getName(_t)) == 0)
                    return _t;
            }
            return -1;
        }

        /// <summary>
        /// Aggiunge una nuova catena al controllo SimplexBreadCrumbs.
        /// @MdN
        /// ----
        /// Crtd: 15/01/2015
        /// </summary>
        /// <param name="p_Name">Nome visualizzato nelle briciole di pane.</param>
        /// <param name="p_Link">URL della form che deve essere richiamata.</param>
        /// <param name="p_lvl">Posizione di inserimento nella catena.</param>
        /// <returns>Il numero degli elementi della catena ovvero -2 in caso di errore.</returns>
        public int InsertAndBreak(String p_Name, String p_Link, int p_lvl)
        {
            // Caricamento o creazione di una collezione SimplexBreadCrumbs
            if ((MySbc = (simplex_FORM.SimplexBreadCrumbs)(Session[MyNome])) == null)
            {
                MySbc = new simplex_FORM.SimplexBreadCrumbs();
                MySbc.Add("HOME", MyHomeLink);
                Session[MyNome] = MySbc;
            }
            return MySbc.InsertAndBreak(p_Name, p_Link, p_lvl);
        }

        /// <summary>
        /// Aggiunge una nuova catena al controllo SimplexBreadCrumbs.
        /// @MdN
        /// ----
        /// Crtd: 15/01/2015
        /// </summary>
        /// <param name="p_Name">Nome visualizzato nelle briciole di pane.</param>
        /// <param name="p_Link">URL della form che deve essere richiamata.</param>
        /// <returns>Il numero degli elementi della catena.</returns>
        public int Add(String p_Name, String p_Link)
        {
            // Caricamento o creazione di una collezione SimplexBreadCrumbs
            if ((MySbc = (simplex_FORM.SimplexBreadCrumbs)(Session[MyNome])) == null)
            {
                MySbc = new simplex_FORM.SimplexBreadCrumbs();
                MySbc.Add("HOME", MyHomeLink);
                Session[MyNome] = MySbc;
            }
            return this.MySbc.Add(p_Name, p_Link);
        }

        /// <summary>
        /// Aggiunge una nuova catena al controllo SimplexBreadCrumbs.
        /// @MdN
        /// ----
        /// Crtd: 15/01/2015
        /// </summary>
        /// <param name="p_Name">Nome visualizzato nelle briciole di pane.</param>
        /// <param name="p_Link">URL della form che deve essere richiamata.</param>
        /// <param name="p_TBL">Oggetto SQLTable di supporto.</param>
        /// <returns>Il numero degli elementi della catena.</returns>
        public int Add(String p_Name, String p_Link, simplex_ORM.SQLSrv.SQLTable p_TBL)
        {
            // Caricamento o creazione di una collezione SimplexBreadCrumbs
            if ((MySbc = (simplex_FORM.SimplexBreadCrumbs)(Session[MyNome])) == null)
            {
                MySbc = new simplex_FORM.SimplexBreadCrumbs();
                MySbc.Add("HOME", MyHomeLink);
                Session[MyNome] = MySbc;
            }
            return this.MySbc.Add(p_Name, p_Link, p_TBL);
        }

        /// <summary>
        /// Aggiunge una nuova catena al controllo SimplexBreadCrumbs.
        /// @MdN
        /// ----
        /// Crtd: 15/01/2015
        /// </summary>
        /// <param name="p_Name">Nome visualizzato nelle briciole di pane.</param>
        /// <param name="p_Link">URL della form che deve essere richiamata.</param>
        /// <param name="p_TBL">Oggetto SQLTable di supporto.</param>
        /// <param name="p_SessionName">Nome della variabile di sessione che contiene il riferimento all'oggetto SQLTable.</param>
        /// <returns>Il numero degli elementi della catena.</returns>
        public int Add(String p_Name, String p_Link, simplex_ORM.SQLSrv.SQLTable p_TBL, String p_SessionName)
        {
            // Caricamento o creazione di una collezione SimplexBreadCrumbs
            if ((MySbc = (simplex_FORM.SimplexBreadCrumbs)(Session[MyNome])) == null)
            {
                MySbc = new simplex_FORM.SimplexBreadCrumbs();
                MySbc.Add("HOME", MyHomeLink);
                Session[MyNome] = MySbc;
            }
            return this.MySbc.Add(p_Name, p_Link, p_TBL, p_SessionName);
        }

        /// <summary>
        /// Aggiunge una nuova catena al controllo SimplexBreadCrumbs.
        /// @MdN
        /// ----
        /// Crtd: 25/01/2015
        /// </summary>
        /// <param name="p_Name">Nome visualizzato nelle briciole di pane.</param>
        /// <param name="p_Link">URL della form che deve essere richiamata.</param>
        /// <param name="p_TBL">Oggetto SQLTable di supporto.</param>
        /// <param name="p_SessionName">Nome della variabile di sessione che contiene il riferimento all'oggetto SQLTable.</param>
        /// <param name="p_Obj">Oggetto di supporto.</param>
        /// <returns>Il numero degli elementi della catena.</returns>
        public int Add(String p_Name, String p_Link, simplex_ORM.SQLSrv.SQLTable p_TBL, String p_SessionName, object p_Obj)
        {
            // Caricamento o creazione di una collezione SimplexBreadCrumbs
            if ((MySbc = (simplex_FORM.SimplexBreadCrumbs)(Session[MyNome])) == null)
            {
                MySbc = new simplex_FORM.SimplexBreadCrumbs();
                MySbc.Add("HOME", MyHomeLink);
                Session[MyNome] = MySbc;
            }
            return this.MySbc.Add(p_Name, p_Link, p_TBL, p_SessionName, p_Obj);
        }

        /// <summary>
        /// Restituisce una stringa contenente la concatenazione attuale con separatore '->'
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 06/02/2015
        /// </pre>
        /// </summary>
        /// <returns>Restituisce una stringa contenente la concatenazione attuale con separatore</returns>
        public String ToString()
        {
            int _t = 0;
            System.Text.StringBuilder _sb = new System.Text.StringBuilder();
            if (MySbc == null)
                return null;
            for (_t = 0; _t < MySbc.Count; _t++)
            {
                if (_t > 0)
                    _sb.Append("->");
                _sb.Append(MySbc.getName(_t));
            }
            return _sb.ToString();
        }

        #endregion

    }
}