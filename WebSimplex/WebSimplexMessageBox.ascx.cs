﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace WebSimplex
{
    /// <summary>
    /// <p>
    /// Controllo MessageBox: mostra una finestra che consente al sistema di 
    /// mostrare un messaggio all'utente ed attende una risposta.
    /// Sono possibili diverse configurazione della message box:
    /// <li>
    /// <ul>Messaggio semplice OK - Annulla</ul>
    /// <ul>Messaggio di Alert</ul>
    /// <ul>Altro .... </ul>
    /// </li>
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 17/04/2015
    /// Mdfd: 06/12/2015 - correzione in SomeButtoHasBeenClicked. v. 1.0.0.1.
    /// </pre>
    /// </summary>
    public partial class WebSimplexMessageBox : System.Web.UI.UserControl
    {

        public enum BoxType{OkMessage, Alert };

        /// <summary>
        /// Gestore dell'evento di pressione del tasto Ok dell'OkMessageBox
        /// </summary>
        public event EventHandler Evt_OkButton;

        /// <summary>
        /// Gestore dell'evento di pressione del tasto Undo dell'OkMessageBox
        /// </summary>
        public event EventHandler Evt_UndoButton;

        /// <summary>
        /// Gestore dell'evento di pressione del tasto Ok dell'AlertMessageBox
        /// </summary>
        public event EventHandler Evt_AlertButton;

        protected int MyType = (int)BoxType.OkMessage;

        #region ATTRIBUTI PROTETTI

        protected String MyName = "MsgBox";

        protected String MyCssOkMsgClass = "OkMsgBox";

        protected String MyCssAlertClass = "AlertMsgBox";

        protected String MyOkMsgText = null;

        protected String MyOkMsgTextCssStyle = "OkMsgText";

        protected String MyAlertText = null;

        protected String MyAlertTextCssStyle = "AlertText";

        protected String MyOkButtonText = "Ok";

        protected String MyOkButtonCssClass = null;

        protected String MyUndoButtonText = "Annulla";

        protected String MyUndoButtonCssStyle = null;

        protected String MyAlertButtonText = "Ok";

        protected String MyAlertButtonCssStyle = null;

        protected String MyOkMsgImage = null;

        protected String MyOkMessageImageCssClass = "MsgImage";

        protected String MyAlertImage = null;

        protected String MyAlertImageCssClass = "AlertImage";

        protected Boolean MyVisible = false;

        #endregion

        #region **** Proprietà Pubbliche ****

        /// <summary>
        /// <p>
        /// Nome del messaggio MessageBox/Alert
        /// (è di utilità e non serve a molto).
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String MessageName
        {
            get
            {
                return MyName;
            }
            set
            {
                if(value!=null)
                    MyName = value;
            }
        }

        /// <summary>
        /// <p>
        /// Nome della ID della classe di stile della MessageBox semplice.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String CssOkMsgClass
        {
            get
            {
                return MyCssOkMsgClass;
            }
            set
            {
                if (value != null)
                    MyCssOkMsgClass = value;
            }
        }

        /// <summary>
        /// Nome della ID della classe di stile della Alert.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String CssAlertClass
        {
            get
            {
                return MyCssAlertClass;
            }
            set
            {
                if (value != null)
                    MyCssAlertClass = value;
            }
        }

        /// <summary>
        /// <p>
        /// Testo del messaggio della Message Box
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String OkMessageText
        {
            get
            {
                return MyOkMsgText;
            }
            set
            {
                if (value != null)
                    MyOkMsgText = value;
            }
        }

        /// <summary>
        /// <p>
        /// Ottiene ed imposta lo stile del testo del messaggio OkMessage.
        /// Il valore di default è "OkMsgText"
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/04/2015
        /// </pre>
        /// </summary>
        public String OkMessageTextCssStyle
        {
            get
            {
                return MyOkMsgTextCssStyle;
            }
            set
            {
                if (value != null)
                    MyOkMsgTextCssStyle = value;
            }
        }

        /// <summary>
        /// <p>
        /// Testo del messaggio Alert.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String AlertText
        {
            get
            {
                return MyAlertText;
            }
            set
            {
                if (value != null)
                    MyAlertText = value;
            }
        }

        /// <summary>
        /// <p>
        /// Ottiene ed imposta lo stile del testo del messaggio Alert.
        /// Il valore di default è "AlertText"
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String AlertTextCssStyle
        {
            get
            {
                return MyAlertTextCssStyle;
            }
            set
            {
                if (value != null)
                    MyAlertTextCssStyle = value;
            }
        }

        /// <summary>
        /// <p>
        /// Testo del tasto OK del Message Box.
        /// Il valore di default è proprio "OK".
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String OkMessageButtonText
        {
            get
            {
                return MyOkButtonText;
            }
            set
            {
                if (value != null)
                    MyOkButtonText = value;
            }
        }

        /// <summary>
        /// <p>
        /// Classe di stile del tasto OK del Message Box.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/04/2015
        /// </pre>
        /// </summary>
        public String OkMessageButtonCssStyle
        {
            get
            {
                return MyOkButtonCssClass;
            }
            set
            {
                MyOkButtonCssClass = value;
            }
        }

        /// <summary>
        /// <p>
        /// Testo del tasto OK dell'Alert.
        /// Il valore di default è proprio "OK".
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String OkAlertButtonText
        {
            get
            {
                return MyAlertButtonText;
            }
            set
            {
                if (value != null)
                    MyAlertButtonText = value;
            }
        }

        /// <summary>
        /// <p>
        /// Stile del tasto OK del messaggio Alert.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/04/2015
        /// </pre>
        /// </summary>
        public String OkAlertButtonCssStyle
        {
            get
            {
                return MyAlertButtonCssStyle;
            }
            set
            {
                MyAlertButtonCssStyle = value;
            }
        }

        /// <summary>
        /// <p>
        /// Testo del tasto "undo" della message box.
        /// Il valore di default è "Annulla".
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String UndoMessageButtonText
        {
            get
            {
                return MyUndoButtonText;
            }
            set
            {
                if (value != null)
                    MyUndoButtonText = value;
            }
        }

        /// <summary>
        /// <p>
        /// Classe di stile del tasto "undo" della message box.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/04/2015
        /// </pre>
        /// </summary>
        public String UndoMessageButtonCssStyle
        {
            get
            {
                return MyUndoButtonCssStyle;
            }
            set
            {
                MyUndoButtonCssStyle = value;
            }
        }


        /// <summary>
        /// <p>
        /// Path dell'eventuale immagine del Message Box.
        /// Se null, non viene inserita alcuna immagine.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String OkMessageImage
        {
            get
            {
                return MyOkMsgImage;
            }
            set
            {
                MyOkMsgImage = value;
            }
        }

        /// <summary>
        /// <p>
        /// Classe di stile css dell'immagine del Message Box.
        /// Il valore di default è "MsgImage"
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/04/2015
        /// </pre>
        /// </summary>
        public String OkMessageImageCssClass
        {
            get
            {
                return MyOkMessageImageCssClass;
            }
            set
            {
                if(value!=null)
                    MyOkMessageImageCssClass = value;
            }
        }


        /// <summary>
        /// <p>
        /// Path dell'eventuale immagine dell'alert box.
        /// Se null, non viene inserita alcuna immagine.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String AlertImage
        {
            get
            {
                return MyAlertImage;
            }
            set
            {
                MyAlertImage = value;
            }
        }

        /// <summary>
        /// <p>
        /// Classe di stile Css dell'eventuale immagine dell'alert box.
        /// Il valore di default è "AlertImage".
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        public String AlertImageCssClass
        {
            get
            {
                return MyAlertImageCssClass;
            }
            set
            {
                if(value!=null)
                    MyAlertImageCssClass = value;
            }
        }

        /// <summary>
        /// <p>
        /// Imposta la visibilità del controllo.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/04/2015
        /// </pre>
        /// </summary>
        //public Boolean Visible
        //{
        //    get
        //    {
        //        return MyVisible;
        //    }
        //    set
        //    {
        //        MyVisible = value;
        //    }
        //}
        
        #endregion

        #region **** Proprietà Private ****
        /// <summary>
        /// Controllo FONDAMENTALE di tipo MultiView che dovrà gestire i diversi messaggi
        /// </summary>
        private MultiView MyMV;
        #endregion

        public void show(int p_type)
        {
            MyType = p_type;
            this.MyMV.ActiveViewIndex = p_type;
            //drawMessageBox(MyType);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            drawMessageBoxes();
        }

        protected virtual void drawMessageBoxes()
        {
            Literal _ltr;
            System.Text.StringBuilder _sb;
            Button _button;
            Image _img;
            /* Creo i due message box in due pannelli alternativi */
            MyMV = new MultiView();
            MyMV.ID = "MyMV";
            View _view = new View();


            /* ***********************
             *      OKMessageBox     *
             *************************/            
                //Apertura
                _ltr = new Literal();
                _sb = new System.Text.StringBuilder("<div id=\"");
                _sb.Append(MyCssOkMsgClass).Append("\">");
                _ltr.Text = _sb.ToString();
                _view.Controls.Add(_ltr);
                //Eventuale immagine
                if (MyOkMsgImage != null)
                {
                    _img = new Image();
                    _img.ImageUrl = MyOkMsgImage;
                    _img.CssClass = MyOkMessageImageCssClass;
                    _view.Controls.Add(_img);
                }
                //Testo
                _ltr = new Literal();
                _sb = new System.Text.StringBuilder("<span class=\"").Append(MyOkMsgTextCssStyle);
                _sb.Append("\">").Append(MyOkMsgText).Append("<span><br>");
                //_ltr.Text = MyOkMsgText + "<br>";
                _ltr.Text = _sb.ToString();
                _view.Controls.Add(_ltr);

                //Bottone OK
                _button = new Button();
                _button.Text = MyOkButtonText;
                if (MyOkButtonCssClass != null)
                    _button.CssClass = MyOkButtonCssClass;
                _button.Click += new EventHandler(SomeButtonHasBeenClicked);
                _view.Controls.Add(_button);

                //Bottone Undo
                _button = new Button();
                _button.Text = MyUndoButtonText;
                if (MyUndoButtonCssStyle != null)
                    _button.CssClass = MyUndoButtonCssStyle;
                _button.Click += new EventHandler(SomeButtonHasBeenClicked);
                _view.Controls.Add(_button);

                //Chiusura del div
                _ltr = new Literal();
                _ltr.Text = "</div>";
                _view.Controls.Add(_ltr);
                
                //Aggiunta della View OkMessageBox
                _view.ID = "OkMessageBox";
                MyMV.Views.Add(_view);

                
                _view = new View();
                /* ***********************
                 *   AlertMessageBox     *
                *************************/            
                //Apertura
                _ltr = new Literal();
                _sb=new System.Text.StringBuilder("<div id=\"");
                _sb.Append(MyCssAlertClass).Append("\">");
                _ltr.Text = _sb.ToString();
                _view.Controls.Add(_ltr);
                //Eventuale immagine
                if (MyAlertImage != null)
                {
                    _img = new Image();
                    _img.ImageUrl = MyAlertImage;
                    _img.CssClass = MyAlertImageCssClass;
                    _view.Controls.Add(_img);
                }
                //Testo
                _ltr = new Literal();
                _sb = new System.Text.StringBuilder("<span class=\"").Append(MyAlertTextCssStyle);
                _sb.Append("\">").Append(MyAlertText).Append("<span><br>");
                _ltr.Text = _sb.ToString();
                //_ltr.Text = MyAlertText + "<br>";
                _view.Controls.Add(_ltr);

                //Bottone OK
                _button = new Button();
                _button.Text = MyAlertButtonText;
                if (MyAlertButtonCssStyle != null)
                    _button.CssClass = MyAlertButtonCssStyle;
                _button.Click += new EventHandler(SomeButtonHasBeenClicked);
                _view.Controls.Add(_button);

                //Chiusura del div
                _ltr = new Literal();
                _ltr.Text = "</div>";
                _view.Controls.Add(_ltr);

                //Aggiunta della View AlertMessageBox
                _view.ID = "AlertMessageBox";
                MyMV.Views.Add(_view);

            this.Controls.Add(MyMV);            
        }
        

        /// <summary>
        /// <p>
        /// Gestore di un evento di pressione su un qualsiasi tasto del messaggio.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 18/04/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="E"></param>
        protected virtual void SomeButtonHasBeenClicked(object sender, EventArgs E)
        {
            //Leggo dalla sessione i delegati per gli eventi e cancello la sessione.   
            if((EventHandler)Session["Evt_OkButton"]!=null)
                Evt_OkButton = (EventHandler)Session["Evt_OkButton"];
            Session["Evt_OkButton"] = null;

            if ((EventHandler)Session["Evt_UndoButton"] != null)
                Evt_UndoButton = (EventHandler)Session["Evt_UndoButton"];
            Session["Evt_UndoButton"] = null;

            if ((EventHandler)Session["Evt_AlertButton"] != null)
                Evt_AlertButton = (EventHandler)Session["Evt_AlertButton"];
            Session["Evt_AlertButton"] = null; ;

            this.Visible = false;

            //Spostare il controllo sul modulo che esegue l'operazione confermata
            if (Evt_OkButton != null && ((System.Web.UI.WebControls.Button)sender).Text.CompareTo(MyOkButtonText) == 0) //<mdfd @MdN: 06/12/2015>
                Evt_OkButton(this, E);
            if (Evt_UndoButton != null && ((System.Web.UI.WebControls.Button)sender).Text.CompareTo(MyUndoButtonText) == 0) //<mdfd @MdN: 06/12/2015>
                Evt_UndoButton(this, E);
            if (Evt_AlertButton != null && ((System.Web.UI.WebControls.Button)sender).Text.CompareTo(MyAlertButtonText) == 0) //<mdfd @MdN: 06/12/2015>
                Evt_AlertButton(this, E);           
        }
    }
}